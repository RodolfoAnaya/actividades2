import { Component, OnInit } from '@angular/core';
import { Recipe } from './receta.model';
import { RecipeService } from './receta.service';

@Component({
  selector: 'app-recetas',
  templateUrl: './recetas.component.html',
  styleUrls: ['./recetas.component.css'],
  providers: [RecipeService]
})
export class RecetasComponent implements OnInit {
  selectedRecipe: Recipe;

  constructor(private recipeService: RecipeService) {
    //this.selectedRecipe=selectedRecipe;
    //En caso de que no configuremos "strictPropertyInitialization": false, en app.module
   }

  ngOnInit(){
    this.recipeService.recipeSelected
    .subscribe(
      (recipe: Recipe) => {
        this.selectedRecipe = recipe;
      }
    );
  }

}
