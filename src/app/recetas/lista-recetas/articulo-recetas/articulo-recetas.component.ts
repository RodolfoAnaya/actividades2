import { Component, Input, OnInit } from '@angular/core';
import { Recipe } from '../../receta.model';

@Component({
  selector: 'app-articulo-recetas',
  templateUrl: './articulo-recetas.component.html',
  styleUrls: ['./articulo-recetas.component.css']
})
export class ArticuloRecetasComponent implements OnInit {
  @Input() recipe: Recipe;
  @Input() index: number;

  ngOnInit() {
  }

}
