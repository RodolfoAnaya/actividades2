import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
//import {HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { RecetasComponent } from './recetas/recetas.component';
import { ListaRecetasComponent } from './recetas/lista-recetas/lista-recetas.component';
import { DetalleRecetasComponent } from './recetas/detalle-recetas/detalle-recetas.component';
import { ArticuloRecetasComponent } from './recetas/lista-recetas/articulo-recetas/articulo-recetas.component';
import { ListaComprasComponent } from './lista-compras/lista-compras.component';
import { EdicionListaComprasComponent } from './lista-compras/edicion-lista-compras/edicion-lista-compras.component';
import { DropdownDirective } from './shared/dropdown.directive';
import { ShoppingListService } from './lista-compras/lista-compras.service';
import { AppRoutingModule } from './app-routing.module';
import { RecetasInicioComponent } from './recetas/recetas-inicio/recetas-inicio.component';
import { RecipeEditComponent } from './recipes/recipe-edit/recipe-edit.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    RecetasComponent,
    ListaRecetasComponent,
    DetalleRecetasComponent,
    ArticuloRecetasComponent,
    ListaComprasComponent,
    EdicionListaComprasComponent,
    DropdownDirective,
    RecetasInicioComponent,
    RecipeEditComponent
    
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
  ],
  providers: [ShoppingListService],
  bootstrap: [AppComponent]
})
export class AppModule { }
