import { EventEmitter, Injectable } from "@angular/core";
import { ShoppingListService } from "../lista-compras/lista-compras.service";
import { Ingredient } from "../shared/ingredient.model";
import { Recipe } from "./receta.model";

@Injectable()
export class RecipeService {
    recipeSelected=new EventEmitter<Recipe>();
    private recipes: Recipe[]=[
        new Recipe('TomaHawk', 'Receta de un tomahawk',
        'https://r.btcdn.co/306/large/564353-Tomahawk-min.jpg',  [
            new Ingredient('Carne', 1),
            new Ingredient('papas', 3),
        ],),
        new Recipe('TomaHawk de Oro', 'Receta de un tomahawk de Oro',
        'https://r.btcdn.co/306/large/564353-Tomahawk-min.jpg', [
            new Ingredient('Carne', 1),
            new Ingredient('papas', 3),
            new Ingredient('oro', 1),
        ])
    ];

    constructor(private slService: ShoppingListService) {}

    getRecipes(){
        return this.recipes.slice();
    }

    getRecipe(index: number){
        return this.recipes[index];
    }

    addIngredientsThoShoppingList(ingredients: Ingredient[]){
        this.slService.addIngredients(ingredients);
    }
}