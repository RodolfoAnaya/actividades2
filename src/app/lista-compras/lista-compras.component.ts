import { Component, OnInit } from '@angular/core';
import { Ingredient } from '../shared/ingredient.model';
import { ShoppingListService } from './lista-compras.service';

@Component({
  selector: 'app-lista-compras',
  templateUrl: './lista-compras.component.html',
  styleUrls: ['./lista-compras.component.css']
})
export class ListaComprasComponent implements OnInit {
  ingredients: Ingredient[];
  constructor( private slService: ShoppingListService) { 
    //this.ingredients=[];
    //En caso de que no configuremos "strictPropertyInitialization": false, en app.module
  }

  ngOnInit() {
    this.ingredients=this.slService.getIngredients();
    this.slService.ingredientsChanged.subscribe(
      (ingredients: Ingredient[])=>{
        this.ingredients=ingredients;
      }
    );
  }
}
