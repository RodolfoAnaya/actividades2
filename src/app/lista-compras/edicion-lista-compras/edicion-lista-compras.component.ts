import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Ingredient } from 'src/app/shared/ingredient.model';
import { ShoppingListService } from '../lista-compras.service';

@Component({
  selector: 'app-edicion-lista-compras',
  templateUrl: './edicion-lista-compras.component.html',
  styleUrls: ['./edicion-lista-compras.component.css']
})
export class EdicionListaComprasComponent implements OnInit {

  constructor(private slService: ShoppingListService) { }

  ngOnInit() {
  }

  onAddItem(form:NgForm){
    const value=form.value
    const newIngredient=new Ingredient(value.name,value.amount);
    this.slService.addIngredient(newIngredient);
  }

}
