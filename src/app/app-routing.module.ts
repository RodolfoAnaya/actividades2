import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { ListaComprasComponent } from "./lista-compras/lista-compras.component";
import { DetalleRecetasComponent } from "./recetas/detalle-recetas/detalle-recetas.component";
import { RecetasInicioComponent } from "./recetas/recetas-inicio/recetas-inicio.component";
import { RecetasComponent } from "./recetas/recetas.component";
import { RecipeEditComponent } from "./recipes/recipe-edit/recipe-edit.component";

const appRoutes: Routes=[
    { path: '',redirectTo: '/recetas', pathMatch: 'full' },
    { path: 'recetas',component: RecetasComponent, children: [
        { path: '',component: RecetasInicioComponent },
        { path: ':id',component: DetalleRecetasComponent},
        { path: 'new', component: RecipeEditComponent },
        { path: ':id/edit', component: RecipeEditComponent }
    ] },
    { path: 'lista-compras',component: ListaComprasComponent },
];

@NgModule({
    imports: [RouterModule.forRoot(appRoutes)],
    exports: [RouterModule]
})
export class AppRoutingModule{

}